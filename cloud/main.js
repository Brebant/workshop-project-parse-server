
Parse.Cloud.define('hello', function(req, res) {
  return 'Hi';
});


Parse.Cloud.define('coucou', function(req, res) {
const fs = require('fs');
const pdf = require('pdf-parse');

let dataBuffer = fs.readFileSync('public/assets/test.pdf');

pdf(dataBuffer).then(function(data) {

    // number of pages
    console.log(data.numpages);
    // number of rendered pages
    console.log(data.numrender);
    // PDF info
    console.log(data.info);
    // PDF metadata
    console.log(data.metadata); 
    // PDF.js version
    console.log(data.version);
    // PDF text
    console.log(data.text); 
  });
  return 'hello'
});
